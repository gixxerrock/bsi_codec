#include <assert.h>
#include <stdio.h>
#include "CommonUtil.h"
#include "quantizer.h"
#include "LosslessEncode.h"

static uint16_t imageW = 0;
static uint16_t imageH = 0;
static int NumBlocksW;
static int NumBlocksH;
static int NumPredBlocks;
static int numBlocksAdded = 0;

static uint8_t *controlList = nullptr;
static uint8_t *dctList = nullptr;
static uint32_t histogram[NumQuantTables][16] = { 0 };

#define DoHistogram 1

void LosslessEncodeInit(const int w, const int h)
{
	NumBlocksW = w / PredTileBlockSize;
	NumBlocksH = h / PredTileBlockSize;
	NumPredBlocks = NumBlocksW*NumBlocksH;

	imageW = w;
	imageH = h;

	numBlocksAdded = 0;

	controlList = new uint8_t[NumPredBlocks];
	dctList = new uint8_t[w*h];
}

void LosslessEncodeAddBlock(uint8_t control, int16_t *dctCoeffs)
{
	controlList[numBlocksAdded] = control;
	int quantBank = control >> QuantShift;

	for (int i = 0; i < PredTileBlockSize2; i++)
	{
		int bank = i/DCTSIZE2;

#if DoHistogram
		// update histogram, this can be used to determine optimal zigzagging by quant table
		if (dctCoeffs[i] != 0)
		{
			histogram[quantBank][i % 16]++;
		}
#endif

		int temp = dctCoeffs[bank*DCTSIZE2 + coeffReorderExp[quantBank][i%DCTSIZE2]];

		temp += 128;
		
		// TODO: can be changed to asserts, clipping should happen at quantization stage now
		if (temp > 255)
		{
			printf("Warning dct coeff overflow %d", temp);
			temp = 255;
		}
		if (temp < 0)
		{
			printf("Warning dct coeff underflow %d", temp);
			temp = 0;
		}

		dctList[numBlocksAdded*PredTileBlockSize2 + i] = (int8_t)temp;
	}
	numBlocksAdded++;
}


void WriteBlockDiagnosticFile(void)
{
	FILE *fp_blocks = fopen(MakeFilename(outputDir, (char *)"temp_blocks.txt"), "wt");
	if (!fp_blocks)
	{
		printf("Error opening target temp_blocks.txt in %s\n", outputDir);
		return;
	}

	// write out histogram info
	fprintf(fp_blocks, "Histogram by quantization table \n");
	for (int i = 0; i < 16; i++)
		fprintf(fp_blocks, "%5d ", i);
	fprintf(fp_blocks, "\n");

	for (int i = 0; i < NumQuantTables; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			fprintf(fp_blocks, "%5d ", histogram[i][j]);
		}
		fprintf(fp_blocks, "\n");
	}

	uint8_t *pDct = dctList;
	uint32_t numZeros = 0;
	// loop for predictor code words
	for (int i = 0; i < NumPredBlocks; i++)
	{
		// predictor code
		int8_t predCode = controlList[i] & PredCodeMask;
		
		// quantization table
		int8_t bank = controlList[i] >> QuantShift;
		
		// write out text file
		fprintf(fp_blocks, "pred: %d   bank: %d (%s)\n", predCode, bank, GetQuantBankDesc(bank));
		for (int j = 0; j < 4; j++)
		{
			for (int k = 0; k < 16; k++)
			{
				int16_t temp = *pDct++ - 128;
				char sep = k % 4 == 0 ? '|' : ' ';
				fprintf(fp_blocks, "%c%4d ",sep, temp);
				if (temp == 0)
					numZeros++;
			}
			fprintf(fp_blocks, "\n");
		}
	}

	fprintf(fp_blocks, "Number Zeros: %d\n", numZeros);
	fclose(fp_blocks);
}

void WriteBlockDCTBlocks(FILE *f1, FILE *f2, FILE *f3, FILE *f4, uint8_t *dctList)
{
	assert(PredDctRatio == 4);
	
	// average DC coefficient for a tile (group of blocks)
	int avgDc = 0;
	for (int b = 0; b < PredDctRatio;b++)
		avgDc += dctList[b*DCTSIZE2];
	avgDc /= PredDctRatio;
	uint8_t avgDc8 = (uint8_t)(avgDc);
	
	fwrite(&avgDc8, 1, 1, f1);

	// each tile contains a number of blocks, with a common predictor	
	for (int b = 0; b < PredDctRatio; b++)
	{
		int numTrailZero =  0;

		// adjust dc coefficient by the average
		dctList[b*DCTSIZE2] -= avgDc8 + 128;

		// count up how many trailing 0s in block (don't count DC)
		for (int i = DCTSIZE2 - 1; i >= 0; i--)
		{
			// note, a zero has been offset to 128
			if (dctList[b*DCTSIZE2 + i] != 0x80)
				break;
			numTrailZero++;
		}
		// num trailing 0s
		fwrite(&numTrailZero, 1, 1, f2);

		// check special case where all 16 coeff are 0, write nothing
		if (numTrailZero != DCTSIZE2)
		{
			// dc coefficient
			fwrite(&dctList[b*DCTSIZE2], 1, 1, f3);
		
			// variable size block of leading data
			fwrite(&dctList[b*DCTSIZE2 + 1], DCTSIZE2 - numTrailZero - 1, 1, f4);
		}
	}
}

void LosslessEncodeDo(void)
{
#if DoHistogram
	WriteBlockDiagnosticFile();
#endif

	FILE *fp_c1 = fopen(MakeFilename(outputDir, (char *)"temp_cont1.bin"), "wb");
	FILE *fp_c2 = fopen(MakeFilename(outputDir, (char *)"temp_cont2.bin"), "wb");
	FILE *fp_d1 = fopen(MakeFilename(outputDir, (char *)"temp_dct1.bin"), "wb");	// average DC for 4 blocks
	FILE *fp_d2 = fopen(MakeFilename(outputDir, (char *)"temp_dct2.bin"), "wb");	// number of trailing 0 coefficients
	FILE *fp_d3 = fopen(MakeFilename(outputDir, (char *)"temp_dct3.bin"), "wb");	// dc - avgDC calculated above
	FILE *fp_d4 = fopen(MakeFilename(outputDir, (char *)"temp_dct4.bin"), "wb");	// remainder of non zero coeff

	if (!(fp_c1 && fp_c2 && fp_d1 && fp_d2 && fp_d3 && fp_d4))
	{
		printf("Error opening target .bin files in %s\n", outputDir);
		return;
	}

	// first write width and height into code word bin ( a little hacky...)
	fwrite(&imageW, sizeof(uint16_t), 1, fp_c1);
	fwrite(&imageH, sizeof(uint16_t), 1, fp_c1);
	fwrite(&gQFactor, sizeof(float), 1, fp_c1);

	// loop for predictor code words
	for (int i = 0; i < NumPredBlocks; i++)
	{
		// predictor code
		int8_t predCode = controlList[i] & PredCodeMask;
		fwrite(&predCode, 1, 1, fp_c1);

		// quantization table
		int8_t bank = controlList[i] >> QuantShift;
		fwrite(&bank, 1, 1, fp_c2);
	}

	// loop for DCT blocks
	for (int i = 0; i < NumPredBlocks; i++)
	{
		WriteBlockDCTBlocks(fp_d1, fp_d2, fp_d3, fp_d4, &dctList[i*DCTSIZE2*PredDctRatio]);
	}

	fclose(fp_c1);
	fclose(fp_c2);
	fclose(fp_d1);
	fclose(fp_d2);
	fclose(fp_d3);
	fclose(fp_d4);

	delete[]controlList;
	delete[]dctList;
}



#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "CommonUtil.h"
#include "stb_image_write.h"
#include "quantizer.h"
#include "dct.h"
#include "Predictor.h"
#include "Decode.h"

uint8_t *LoadFile(size_t *size, char *fn)
{
	FILE *fp = fopen(fn, "rb");
	if (!fp)
	{
		*size = 0;
		return nullptr;
	}

	// get size
	fseek(fp, 0L, SEEK_END);
	*size = ftell(fp);
	rewind(fp);

	uint8_t *data = new uint8_t[*size];
	fread(data, *size, 1, fp);
	
	fclose(fp);
	return data;
}

void DecodeImage(void)
{
	size_t size_c1;
	size_t size_c2;
	size_t size_d1;
	size_t size_d2;
	size_t size_d3;
	size_t size_d4;
	uint8_t *data_c1 = LoadFile(&size_c1, MakeFilename(decodeInputDir, (char *)"temp_cont1.bin"));
	uint8_t *data_c2 = LoadFile(&size_c2, MakeFilename(decodeInputDir, (char *)"temp_cont2.bin"));
	uint8_t *data_d1 = LoadFile(&size_d1, MakeFilename(decodeInputDir, (char *)"temp_dct1.bin"));
	uint8_t *data_d2 = LoadFile(&size_d2, MakeFilename(decodeInputDir, (char *)"temp_dct2.bin"));
	uint8_t *data_d3 = LoadFile(&size_d3, MakeFilename(decodeInputDir, (char *)"temp_dct3.bin"));
	uint8_t *data_d4 = LoadFile(&size_d4, MakeFilename(decodeInputDir, (char *)"temp_dct4.bin"));

	if (!(size_c1 && size_c2 && size_d1 && size_d2 && size_d3 && size_d4))
	{
		printf("Error opening target .bin files in %s\n", decodeInputDir);
		return;
	}

	// a bit hacky, but recover width and height as the first 4 bytes
	uint16_t w;
	uint16_t h;
	memcpy(&w, data_c1, 2);
	memcpy(&h, data_c1 + 2, 2);
	memcpy(&gQFactor, data_c1 + 4, 4);

	InitQuantTables(gQFactor);

	const int NumCodeBlocks = w*h / PredTileBlockSize2;
	const int NumDctBlocks = NumCodeBlocks*PredDctRatio;

	assert(size_c1 - 8 == NumCodeBlocks);
	assert(size_c2 == NumCodeBlocks);
	assert(size_d1 == NumCodeBlocks);

	// reassemble control
	uint8_t *cont = new uint8_t[NumCodeBlocks];
	uint8_t *cont1 = data_c1 + 8;
	for (int i = 0; i < NumCodeBlocks; i++)
		cont[i] = (data_c2[i] << 5) | cont1[i];

	int numDcCoeffUsed = 0;			// need to track because it is variable sized
	int numCoeffUsed = 0;
	int16_t *dctList = new int16_t[w*h];
	memset(dctList, 0, sizeof(int16_t)*w*h);

	// reassemble quantized dct coeff  *Note the coefficients are swizzled, and in groups of 4 blocks
	for (int i = 0; i < NumCodeBlocks; i++)
	{
		int8_t baseDC = data_d1[i] - 128;

		// this section is hard coded for 4 4x4 dct blocks fitting inside one 8x8 pred block
		for (int subBlock = 0; subBlock < PredDctRatio; subBlock++)
		{
			int blockNum = i*PredDctRatio + subBlock;
			int baseIdx = blockNum * DCTSIZE2;
			int numTrailingZero = data_d2[i*PredDctRatio + subBlock];

			// put in base DC value
			dctList[baseIdx] = baseDC;

			// special case where they were all zero
			if (numTrailingZero == DCTSIZE2)
				continue;
			
			//add in DC value
			dctList[baseIdx] += data_d3[numDcCoeffUsed++] - 128;

			for (int c = 0; c < DCTSIZE2 - numTrailingZero - 1; c++)
			{
				int dctOffs = coeffReorderExp[data_c2[i]][c + 1];
				dctList[baseIdx + dctOffs] = data_d4[numCoeffUsed++] -128;
			}
		}
	}

	DecodeImage(w, h, cont, dctList);

	delete[]cont;
	delete[]dctList;
	delete[]data_c1;
	delete[]data_c2;
	delete[]data_d1;
	delete[]data_d2;
	delete[]data_d3;
	delete[]data_d4;
}			

void DecodeImage(int w, int h, uint8_t *cont, int16_t *dctList)
{
	const int numPredBlocksW = w / PredTileBlockSize;
	const int numPredBlocks = numPredBlocksW*h / PredTileBlockSize;

	uint8_t *decodedRes = new uint8_t[w*h];
	uint8_t *decodedImg = new uint8_t[w*h];

	for(int predBlock = 0;predBlock<numPredBlocks;predBlock++)
	{
		for (int dctBlock = 0; dctBlock < PredDctRatio; dctBlock++)
		{
			uint8_t code = cont[predBlock];

			int rowOffs = dctBlock / PredCodeReduction;
			int colOffs = dctBlock % PredCodeReduction;
			int row = PredTileBlockSize*(predBlock / numPredBlocksW) + rowOffs*DCTSIZE;
			int col = PredTileBlockSize*(predBlock % numPredBlocksW) + colOffs*DCTSIZE;

			// turn dct coded into our recovered residual
			int16_t temp[DCTSIZE][DCTSIZE];// stupid, till I figure out how to do typecast
			memcpy(temp, &dctList[(predBlock*PredDctRatio + dctBlock)*DCTSIZE2], DCTSIZE2*sizeof(int16_t));
			DCTDecodeBlock(temp, decodedRes, w, row, col, code);

			// apply predictor to reconstruct original image
			PredictorDecodeBlock(decodedRes, code, decodedImg, row, col, w);
		}
	}

	stbi_write_png(MakeFilename(outputDir, (char *)"decodedRes.png"), w, h, 1, decodedRes, w);
	stbi_write_png(outFileName, w, h, 1, decodedImg, w);

	delete[]decodedRes;
	delete[]decodedImg;
}
#include <math.h>
#include <assert.h>
#include "quantizer.h"


#if (DCTSIZE == 8)
// copied from jcparam.c
unsigned int std_luminance_quant_tbl[DCTSIZE2] = {
	16,  11,  10,  16,  24,  40,  51,  61,
	12,  12,  14,  19,  26,  58,  60,  55,
	14,  13,  16,  24,  40,  57,  69,  56,
	14,  17,  22,  29,  51,  87,  80,  62,
	18,  22,  37,  56,  68, 109, 103,  77,
	24,  35,  55,  64,  81, 104, 113,  92,
	49,  64,  78,  87, 103, 121, 120, 101,
	72,  92,  95,  98, 112, 100, 103,  99
};

const float horz_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.1f, 1.2f, 1.2f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.2f, 1.5f, 1.5f, 1.3f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.8f, 2.0f, 2.0f, 1.2f, 1.0f, 1.0f, 1.0f, 1.0f,
	2.1f, 2.0f, 1.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	2.0f, 1.7f, 1.2f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
};

const float vert_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f, 1.2f, 1.3f, 2.0f, 2.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.3f, 1.5f, 2.0f, 1.3f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.3f, 2.0f, 1.5f, 1.2f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.2f, 1.2f, 1.2f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
};

const float mid_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.5f, 1.5f, 1.5f, 1.5f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.5f, 1.8f, 1.8f, 1.5f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.5f, 1.8f, 1.8f, 1.5f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.5f, 1.5f, 1.5f, 1.5f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
};

// TODO: temp dummy pointers, since not yet implemented
const float *lf_boost_quant = mid_boost_quant;
const float *hf_boost_quant = mid_boost_quant;

#else
unsigned int std_luminance_quant_tbl[DCTSIZE2] = {
	10,  30,  54,  84,
	30,  20,  81,  92,
	54,  87,  85, 106,
	92,  96, 106,  99
};

const float lf_boost_quant[DCTSIZE2] = {
	1.4f, 1.2f, 1.0f, 1.0f,
	1.2f, 1.2f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f
};

const float vert_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.5f, 1.5f, 1.0f, 1.0f,
	1.5f, 1.5f, 1.0f, 1.0f
};

const float horz_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.5f, 1.5f,
	1.0f, 1.0f, 1.5f, 1.5f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f
};

const float mid_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.5f, 1.5f, 1.0f,
	1.0f, 1.5f, 1.5f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f
};

const float hf_boost_quant[DCTSIZE2] = {
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.4f, 1.5f,
	1.0f, 1.0f, 1.5f, 1.5f
};

#endif
// this uses either the standard jpeg quantization table, or a home brew (much flatter) one I experimentally
// tweaked to work better on residual images that have had low freq energy sucked out.  The "q" is a number
// to scale up the tables, making the quantization more agressive.  Good range seems to be 1..30 (best..worst qual)
static float quantTable[NumQuantTables][DCTSIZE][DCTSIZE];
static int quantTableJpeg[DCTSIZE][DCTSIZE];
static float quantTableQ = 0.0f;

void InitQuantTables(float q)
{
	quantTableQ = q;
	// only set up to handle 5 right now (hard coded)
	assert(NumQuantTables == 5);

	// numbers for stock jpeg quantizer table
	int scale = (int)q;
	scale = scale > 100 ? 100 : scale;
	scale = scale < 1 ? 1 : scale;
	if (scale < 50)
		scale = 5000 / scale;
	else
		scale = 200 - scale * 2;

	float baseQuant[DCTSIZE][DCTSIZE];
	for (int r = 0; r < DCTSIZE; r++)
		for (int c = 0; c < DCTSIZE; c++)
		{
			baseQuant[r][c] = std_luminance_quant_tbl[r*DCTSIZE + c] * q / 16.0f;

			quantTable[0][r][c] = baseQuant[r][c] / lf_boost_quant[r*DCTSIZE + c];
			quantTable[1][r][c] = baseQuant[r][c] / horz_boost_quant[r*DCTSIZE + c];
			quantTable[2][r][c] = baseQuant[r][c] / vert_boost_quant[r*DCTSIZE + c];
			quantTable[3][r][c] = baseQuant[r][c] / mid_boost_quant[r*DCTSIZE + c];
			quantTable[4][r][c] = baseQuant[r][c] / hf_boost_quant[r*DCTSIZE + c];

			// standard jpeg table
			int temp = (std_luminance_quant_tbl[r*DCTSIZE + c] * scale + 50) / 100;
			temp = temp < 1 ? 1 : temp;
			temp = temp > 32767 ? 32767 : temp;
			quantTableJpeg[r][c] = temp;
		}
}

char *GetQuantBankDesc(int bank)
{
	static char* BankDesc[] = { "lf_boost", "horz_boost", "vert_boost", "mid_boost", "hf_boost" };
	return BankDesc[bank];
}

void QuantizeBlock(float in[][DCTSIZE], int16_t out[][DCTSIZE], uint8_t code)
{
	int tabIdx = code >> QuantShift;

	for (int y = 0; y < DCTSIZE; y++)
		for (int x = 0; x < DCTSIZE; x++)
		{
			float temp = roundf(in[y][x] / quantTable[tabIdx][y][x]);
			int16_t iTemp = (int16_t)round(temp);

			// simulate clipping from going to byte
			if (iTemp < -128)
				iTemp = -128;
			if (iTemp > 127)
				iTemp = 127;

			out[y][x] = iTemp;
		}
}


void DeQuantizeBlock(int16_t in[][DCTSIZE], float out[][DCTSIZE], uint8_t code)
{
	int tabIdx = code >> QuantShift;
	for (int y = 0; y < DCTSIZE; y++)
		for (int x = 0; x < DCTSIZE; x++)
			out[y][x] = (float)in[y][x] * quantTable[tabIdx][y][x];
}
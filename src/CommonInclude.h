#pragma once
#include <cstdint>

// TODO: DCTSIZE redundant with BlockSize
#if 0
#define DCTSIZE 8

constexpr int PredCodeReduction = 1;
#else
#define DCTSIZE 4
constexpr int PredCodeReduction = 2;
#endif

#define DCTSIZE2 (DCTSIZE*DCTSIZE)
constexpr int BlockSize = DCTSIZE;
constexpr int PredTileBlockSize = BlockSize*PredCodeReduction;
constexpr int PredTileBlockSize2 = PredTileBlockSize*PredTileBlockSize;
constexpr int PredDctRatio = PredCodeReduction*PredCodeReduction;

constexpr uint8_t PredCodeMask = 0x1f;
constexpr uint8_t QuantCodeMask = 0xe0;
constexpr uint8_t QuantShift = 5;

constexpr int NumCodes = 31;
constexpr int NumQuantTables = 5;

#define MAXPATH 255
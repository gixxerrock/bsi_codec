#pragma once
#include "CommonInclude.h"

void LosslessEncodeInit(const int w, const int h);
void LosslessEncodeAddBlock(uint8_t control, int16_t *dctCoeffs);
void LosslessEncodeDo(void);


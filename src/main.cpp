#include <stdio.h>

#include "CommonUtil.h"
#include "dct.h"
#include "quantizer.h"
#include "Predictor.h"
#include "Decode.h"


// use stb files
#pragma warning(disable:4996)
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_write.h"

extern unsigned int std_luminance_quant_tbl[DCTSIZE2];

uint8_t* LoadLumFile(char* filename, int* w, int* h)
{
	int n;
	unsigned char * srcImg = stbi_load(filename, w, h, &n, 0);
	if (!srcImg)
	{
		printf("Error opening %s\n", filename);
		return nullptr;
	}
	printf("Loaded %s  %dx%d \n", filename, *w, *h);

	unsigned char* lum = StripChannel(srcImg, *w, *h, 0, n);
	free(srcImg);
	
	return lum;
}

void TestPredictor(char* filename)
{
	// load file
	int w, h;
	uint8_t *lum = LoadLumFile(filename, &w, &h);

	if (lum)
	{
		PredictorEncode(lum, w, h);
		//LosslessPredictorEncode(lum, w, h);

		free(lum);
	}
}

void RunDecoder(void)
{
	int w1, h1;
	uint8_t *resImg = LoadLumFile((char*)"temp/residual.jpg", &w1, &h1);

	int w2, h2;
	uint8_t *codeImg = LoadLumFile((char*)"temp/control.png", &w2, &h2);

	PredictorDecode(codeImg, resImg, w1, h1, nullptr);

	free(resImg);
	free(codeImg);
}

void TestDCTTransforms(char *filename)
{
	// load file
	int w, h;
	uint8_t *lum = LoadLumFile(filename, &w, &h);

	TestDCTTransforms(lum, w, h);
}


void MakeQuantTable1(char *fn)
{
	FILE *fp = fopen(fn, "wt");
	for (int r = 0; r < DCTSIZE; r++)
	{
		for (int c = 0; c < DCTSIZE; c++)
		{
			//float dOffs = 5.0f;
			//float dist = sqrt((dOffs + r)*(dOffs + r) + (dOffs + c)*(dOffs + c));
			fprintf(fp, "33 ");
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void MakeQuantTable2(char *fn)
{
	FILE *fp = fopen(fn, "wt");
	for (int r = 0; r < DCTSIZE; r++)
	{
		for (int c = 0; c < DCTSIZE; c++)
		{
			float dOffs = 6.0f;
			float dist = sqrtf((dOffs + r)*(dOffs + r) + (dOffs + c)*(dOffs + c));
			fprintf(fp, "%d ", (int)(29.0f + 4.0f*dist));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

void TestDCT(char *fn)
{
	// load file
	int w, h;

    uint8_t *lum = LoadLumFile(fn, &w, &h);

	TestDCTTransforms(lum, w, h);

	free(lum);
}

//global variables that can be overriden by cmd line arguments
float gQFactor = 9.0f;
char *inFileName = (char *)"images/GrayKodo/gray_kodim04.png";
char *outFileName = (char *)"temp/decoded.png";
char *outputDir = (char *)"temp";
char *decodeInputDir = (char *)"temp";
bool bDecode = false;
bool bDontRun = false;

char* GetOutputPath(char *outfile)
{
	bool foundPath = false;
	char *dir = nullptr; 
	size_t len = strlen(outfile);
	for (int i = len; i > -1; i--)
	{
		if (!foundPath)
		{
			if (outfile[i] == '\\' || outfile[i] == '/')
			{
				foundPath = true;
				dir = new char[strlen(outfile)];
				dir[i + 1] = '\0';
			}
			else
				continue;
		}

		dir[i] = outfile[i];
	}
	return dir;
}

int ProcessNextArg(int argIdx, char *argv[])
{
	if (strcmp(argv[argIdx], "-q") == 0)
	{
		gQFactor = strtof(argv[argIdx + 1], NULL);
		return 2;
	}
	if (strcmp(argv[argIdx], "-d") == 0)
	{
		decodeInputDir = argv[argIdx + 1];
		bDecode = true;
	}
	
	if (strcmp(argv[argIdx], "-i") == 0)
	{
		inFileName = argv[argIdx + 1];
	}

	if (strcmp(argv[argIdx], "-o") == 0)
	{
		outFileName = argv[argIdx + 1];
		outputDir = GetOutputPath(outFileName);
		if (outputDir == nullptr)
		{
			outputDir = GetOutputPath(argv[0]);
		}
	}

	if (strcmp(argv[argIdx], "-h") == 0)
	{
		printf("Usage: bsi_codec -i infile.png -o outfile.png -q 30.0\n");
		printf("Where q is related to quality 5:good 50:bad default in neighborhood of 30\n");
		printf("or to decode a block of bin files, use:\n");
		printf("bsi_codec -d temp/ -o temp/decodedImage.png\n");
		bDontRun = true;
		return 1;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	int numArgsDone = 1;
	while (numArgsDone < argc)
		numArgsDone += ProcessNextArg(numArgsDone, argv);
	
	if (bDontRun)
		return 0;

	InitDctTables();
	InitQuantTables(gQFactor);

	if (!bDecode)
	{
		// encode
		TestPredictor(inFileName);
		//TestDCT(inFileName);
	}
	else
	{
		DecodeImage();
	}

	if (0)
	{
		TestDCT((char*)"images/GrayKodo/gray_kodim01.png");
		TestDCT((char*)"images/GrayKodo/gray_kodim02.png");
		TestDCT((char*)"images/GrayKodo/gray_kodim04.png");
		TestDCT((char*)"images/GrayKodo/gray_kodim05.png");
	}

	if (0)
	{
		TestPredictor((char*)"images/GrayKodo/gray_kodim01.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim02.png");
		//TestPredictor("images/GrayKodo/gray_kodim03.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim04.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim05.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim06.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim07.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim08.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim09.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim10.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim11.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim12.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim13.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim14.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim15.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim16.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim17.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim18.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim19.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim20.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim21.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim22.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim23.png");
		TestPredictor((char*)"images/GrayKodo/gray_kodim24.png");

		DumpStats();
	}

	if (0)
	{
		//MakeQuantTable1("QuantTab1.txt");
		MakeQuantTable2((char*)"QuantTab2.txt");	// works best
		//MakeQuantTable3("QuantTab3.txt");
	}
}

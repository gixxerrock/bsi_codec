#include <math.h>
#include <string.h>
#include <stdio.h>
#include "CommonUtil.h"

uint8_t* StripChannel(uint8_t* src, int w, int h, int offset, int skip)
{
	unsigned char* dest = new uint8_t[w*h];

	unsigned char* pDst = dest + offset;
	src += offset;
	for (int i = 0; i < w*h; i++)
	{
		*pDst = *src;
		pDst++;
		src += skip;
	}
	return dest;
}

// warning, this is obviously not re-entrant, or made for any kind of long term storage of pathname
char *MakeFilename(char *dir, char *file)
{
	static char fullPath[MAXPATH];

	strcpy(fullPath, dir);
	strcat(fullPath, "/");
	strcat(fullPath, file);

	return fullPath;
}

#if DCTSIZE == 8
int coeffReorder[DCTSIZE2] = { 0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,12,19,26,33,40,48,41,34,27,20,13,6,7,14,21,28,35,42,49,56,57,50,43,36,29,22,15,23,30,37,44,51,58,59,52,45,38,31,39,46,53,60,61,54,47,55,62,63 };

int coeffReorderExp[NumQuantTables][DCTSIZE2] = {
{ 0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,12,19,26,33,40,48,41,34,27,20,13,6,7,14,21,28,35,42,49,56,57,50,43,36,29,22,15,23,30,37,44,51,58,59,52,45,38,31,39,46,53,60,61,54,47,55,62,63 },
{ 0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,12,19,26,33,40,48,41,34,27,20,13,6,7,14,21,28,35,42,49,56,57,50,43,36,29,22,15,23,30,37,44,51,58,59,52,45,38,31,39,46,53,60,61,54,47,55,62,63 },
{ 0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,12,19,26,33,40,48,41,34,27,20,13,6,7,14,21,28,35,42,49,56,57,50,43,36,29,22,15,23,30,37,44,51,58,59,52,45,38,31,39,46,53,60,61,54,47,55,62,63 },
{ 0,1,8,16,9,2,3,10,17,24,32,25,18,11,4,5,12,19,26,33,40,48,41,34,27,20,13,6,7,14,21,28,35,42,49,56,57,50,43,36,29,22,15,23,30,37,44,51,58,59,52,45,38,31,39,46,53,60,61,54,47,55,62,63 } };

#else
//int coeffReorder[DCTSIZE2] = { 0,1,4,8,5,2,3,6,9,12,13,10,7,11,14,15 };
int coeffReorder[DCTSIZE2] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };

// built up by histograms of non-zero coeffients, based on different quantization tables
int coeffReorderExp[NumQuantTables][DCTSIZE2] = {
	{0,4,5,1,8,2,9,6,12,10,3,7,13,14,11,15},
	{0,1,5,4,2,6,8,3,7,10,9,11,12,13,14,15},
	{0,4,5,8,1,9,12,2,13,6,10,7,3,14,11,15},
	{0,5,4,1,6,9,8,10,2,7,12,3,13,11,14,15},
	{0,5,4,1,10,2,8,6,11,9,14,15,7,12,13,3} };

//int coeffReorderExp[NumQuantTables][DCTSIZE2] = {
//	{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 },
//	{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 },
//	{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 },
//	{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 },
//	{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 } };

#endif

void ZigZagCoefficients(int16_t *workspace)
{
	int16_t temp[DCTSIZE2];

	// reorder the coefficients
	for (int i = 0; i < DCTSIZE2; i++)
		temp[i] = workspace[coeffReorder[i]];

	// copy back
	for (int i = 0; i < DCTSIZE2; i++)
		workspace[i] = temp[i];
}

// copy 8x8 block of pixels from src to dst.  ** note** dimensions are original image dimension
void BlitBlock(uint8_t *src, uint8_t *dst, const int startRow, const int startCol, const int w)
{
	for (int row = startRow; row < startRow + PredTileBlockSize; row++)
		for (int col = startCol; col < startCol + PredTileBlockSize; col++)
			dst[w*row + col] = src[w*row + col];
}

float ConvertMSEToPSNR(float a_fMSE)
{
	if (a_fMSE == 0.0f)
	{
		return INFINITY;
	}

	return 10.0f * log10f(1.0f / a_fMSE);
}

float CalculateBlockMSE(const uint8_t *img1, const uint8_t *img2, const int startRow, const int startCol, const int w)
{
	float mse = 0;

	for (int row = startRow; row < startRow + BlockSize; row++)
	{
		for (int col = startCol; col < startCol + BlockSize; col++)
		{
			float err = (float)(img1[w*row + col] - img2[w*row + col]);
			err *= err;
			mse += err;
		}
	}

	return mse / (BlockSize*BlockSize*256.0f*256.0f);
}

float CalculatePSNR(const uint8_t *img1, const uint8_t *img2, const int w, const int h)
{
	int len = w*h;
	float psnr = 0.0f;
	for (int i = 0; i < len; i++)
	{
		float err = (float)(*img1++ - *img2++);
		err *= err;
		psnr += err;
	}
	psnr /= len*256.0f*256.0f;
	psnr = ConvertMSEToPSNR(psnr);

	return psnr;
}

void DumpCodewordStats(uint8_t *control, int numBlocks)
{
	int quantHist[NumQuantTables] = { 0 };
	int codewordHist[NumCodes] = { 0 };

	for (int i = 0; i < numBlocks; i++)
	{
		uint8_t code = control[i];
		quantHist[code >> QuantShift]++;
		codewordHist[code&PredCodeMask]++;
	}

	for (int i = 0; i < NumQuantTables; i++)
		printf("Quant[%d] : %d\n", i, quantHist[i]);
	for (int i = 0; i < NumCodes; i++)
		printf("codeword[%d] : %d\n", i, codewordHist[i]);
}

#pragma once
#include "CommonInclude.h"

int PredictorEncodeDCTBlock(const uint8_t *srcImg, uint8_t *dstImg, const uint8_t code, const int startRow, const int startCol, const int w, int *overflow);
float PredictorEncodeBlock(uint8_t *image, uint8_t *distortedImage, uint8_t *residual, uint8_t *distortedResidual, uint8_t code, const int row, const int col, const int w, bool bDoLossless);
void PredictorDecodeBlock(uint8_t *residual, const uint8_t code, uint8_t* image, const int startRow, const int startCol, const int w);
void PredictorEncode(uint8_t *image, const int w, const int h);
void LosslessPredictorEncode(uint8_t *image, const int w, const int h);
uint8_t* PredictorDecode(uint8_t *control, uint8_t *residual, const int w, const int h, uint8_t *original);

void AccumulateStats(uint8_t *control, uint8_t *residual, const int w, const int h);
void DumpStats(void);
char* GetPredictorDesc(int pred);
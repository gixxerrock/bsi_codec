#include <math.h>
#include <assert.h>
#include <stdio.h>
#include "stb_image_write.h"
#include "CommonUtil.h"
#include "LosslessEncode.h"
#include "quantizer.h"
#include "dct.h"


#define PI  3.14159265358979f
#define InvRt2  0.707106781f

static float dctTable[DCTSIZE][DCTSIZE][DCTSIZE][DCTSIZE];
void InitDctTables(void)
{
	// pre-calc dct tables to save us doing sin/cos at runtime
	for(int v=0;v<DCTSIZE;v++)
		for (int u = 0; u<DCTSIZE; u++)
			for (int y = 0; y<DCTSIZE; y++)
				for (int x = 0; x<DCTSIZE; x++)
					dctTable[v][u][y][x] = cosf((y + 0.5f)*v*PI / DCTSIZE) * cosf((x + 0.5f)*u*PI / DCTSIZE);
}

void DCT_float( float in[][DCTSIZE], float out[][DCTSIZE])
{
	for (int v = 0; v < DCTSIZE; v++)
	{
		float Cv = (v == 0) ? InvRt2 : 1;
		for (int u = 0; u < DCTSIZE; u++)
		{
			float sum = 0.0f;
			float Cu = (u == 0) ? InvRt2 : 1;
			for (int y = 0; y < DCTSIZE; y++)
				for (int x = 0; x < DCTSIZE; x++)
					sum += in[y][x] * dctTable[v][u][y][x];
			
			out[v][u] = (2.0f/DCTSIZE)*Cu*Cv*sum;
		}
	}
}

void IDCT_float(float in[][DCTSIZE], float out[][DCTSIZE])
{
	for (int y = 0; y < DCTSIZE; y++)
	{
		for (int x = 0; x < DCTSIZE; x++)
		{
			float sum = 0.0f;
			
			for (int v = 0; v < DCTSIZE; v++)
			{
				float Cv = (v == 0) ? InvRt2 : 1;
				for (int u = 0; u < DCTSIZE; u++)
				{
					float Cu = (u == 0) ? InvRt2 : 1;
					sum += Cu*Cv*in[v][u] * dctTable[v][u][y][x];
				}
			}
			out[y][x] = (2.0f / DCTSIZE)*sum;
		}
	}
}

void BlockByte2Float(uint8_t *srcImg, const int w, const int r, const int c, float out[][DCTSIZE])
{
	for (int y = 0; y < DCTSIZE; y++)
		for (int x = 0; x < DCTSIZE; x++)
			out[y][x] = srcImg[w*(r+y) + c + x] - 128.0f;
}

void BlockFloat2Byte(float in[][DCTSIZE], uint8_t *dstImg, const int w, const int r, const int c)
{
	for (int y = 0; y < DCTSIZE; y++)
		for (int x = 0; x < DCTSIZE; x++)
		{
			float temp = roundf(in[y][x] + 128.0f);
			temp = (temp < 0.0f) ? 0.0f : temp;
			temp = (temp > 255.0f) ? 255.0f : temp;
			*(dstImg + w*(r + y) + c + x) = (uint8_t)(temp);
		}
}

void DCTEncodeBlock(uint8_t *srcImg, int16_t dctCoeff[][DCTSIZE], int w, int r, int c, uint8_t code)
{
	float f1[DCTSIZE][DCTSIZE];
	float f2[DCTSIZE][DCTSIZE];

	BlockByte2Float(srcImg, w, r, c, f1);
	DCT_float(f1, f2);
	QuantizeBlock(f2, dctCoeff, code);
}

void DCTDecodeBlock(int16_t dctCoeff[][DCTSIZE], uint8_t *dstImg, int w, int r, int c, uint8_t code)
{
	float f1[DCTSIZE][DCTSIZE];
	float f2[DCTSIZE][DCTSIZE];

	DeQuantizeBlock(dctCoeff, f1, code);
	IDCT_float(f1, f2);
	BlockFloat2Byte(f2, dstImg, w, r, c);
}

void TestDCTTransforms(uint8_t *image, const int w, const int h)
{
	uint8_t* decodedImg = new uint8_t[w*h];

	LosslessEncodeInit(w, h);
	for (int row = 0; row < h; row += DCTSIZE)
	{
		for (int col = 0; col < w; col += DCTSIZE)
		{
			int16_t dctCoeff[DCTSIZE][DCTSIZE];
			uint8_t code = 0;
			DCTEncodeBlock(image, dctCoeff, w, row, col, code);
			DCTDecodeBlock(dctCoeff, decodedImg, w, row, col, code);

			LosslessEncodeAddBlock(code, (int16_t *)dctCoeff);
		}
	}
	LosslessEncodeDo();

	// write out image
	stbi_write_png(outFileName, w, h, 1, decodedImg, w);
	delete[] decodedImg;
}

void TestDCTTransforms_old(uint8_t *image, const int w, const int h)
{
	const int numBlocksW = w / DCTSIZE;
	const int numBlocksH = h / DCTSIZE;
	const int NumBlocks = numBlocksW*numBlocksH;
	
	uint8_t* decodedImg = new uint8_t[w*h];
	LosslessEncodeInit(w, h);

	int16_t error[DCTSIZE][DCTSIZE];
	int32_t zeroCnt[DCTSIZE][DCTSIZE] = { {0} };
	int32_t error_avgAbs[DCTSIZE][DCTSIZE] = { {0} };
	int numPerfect = 0;
	int numZeroCoeff = 0;
	int worstError = 0;
	uint8_t code = 0;
	for (int row = 0; row < h; row += DCTSIZE)
	{
		for (int col = 0; col < w; col += DCTSIZE)
		{
			int16_t dctCoeff[DCTSIZE][DCTSIZE];

			DCTEncodeBlock(image, dctCoeff, w, row, col, code);
			DCTDecodeBlock(dctCoeff, decodedImg, w, row, col, code);

			// collect some stats on the block
			for (int i = 0; i < DCTSIZE; i++)
			{
				for (int j = 0; j < DCTSIZE; j++)
				{
					int16_t err = image[(row + i)*w + col + j] - decodedImg[(row + i)*w + col + j];
					error[i][j] = err;
					
					// take absolute value
					err = (err > 0) ? err : -err;
					error_avgAbs[i][j] += err;

					if (err > worstError)
						worstError = err;
					if (!err)
						numPerfect++;

					// stats on quantised freq coefficients
					if (!dctCoeff[i][j])
					{
						zeroCnt[i][j]++;
						numZeroCoeff++;
					}
				}
			}
		}
	}

	printf("********DCT Transform and Lossy Quantization***********\n");
	for (int i = 0; i < DCTSIZE; i++)
	{
		printf("[");
		for (int j = 0; j < DCTSIZE; j++)
			printf("%4d", error_avgAbs[i][j] / NumBlocks);

		printf(" ] [");

		for (int j = 0; j < DCTSIZE; j++)
			printf("%4d", zeroCnt[i][j]*100 / NumBlocks);

		printf(" ]\n");
	}

	float psnr = CalculatePSNR(image, decodedImg, w, h);
	printf("No Err = %.3f PSNR = %.3f zero Coeff %.3f worst err %d \n", (float)numPerfect/(w*h), psnr, (float)numZeroCoeff /(w*h), worstError);

	// write out image
	stbi_write_png(outFileName, w, h, 1, decodedImg, w);
	delete[] decodedImg;
}

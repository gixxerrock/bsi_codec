#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include "stb_image.h"
#include "stb_image_write.h"
#include "CommonUtil.h"
#include "LosslessEncode.h"
#include "Predictor.h"
#include "quantizer.h"
#include "dct.h"
#include "Decode.h"

inline int Average(int y1, int y2)
{
	return ((y1 + y2) >> 1);
}

inline int Velocity(int y1, int y2)
{
	return ((y2 - y1) << 0) + y2;
}

int GetPredictorBase(const uint8_t *srcImg, const uint8_t code, int row, int col, int w)
{
	// f g h 
	// c d e 
	// a b *
	int b = (col > 0) ? srcImg[row*w + col - 1] : 0;
	int a = (col > 1) ? srcImg[row*w + col - 2] : b;

	int e = (row > 0) ? srcImg[(row - 1)*w + col] : 0;
	int d = (row > 0 && col > 0) ? srcImg[(row - 1)*w + col - 1] : e;
	int c = (row > 0 && col > 1) ? srcImg[(row - 1)*w + col - 2] : d;

	int h = (row > 1) ? srcImg[(row - 2)*w + col] : e;
	int g = (row > 1 && col > 0) ? srcImg[(row - 2)*w + col - 1] : h;
	int f = (row > 1 && col > 1) ? srcImg[(row - 2)*w + col - 2] : g;

	int base = 0;
	switch (code)
	{
	case 0:
		base = 0;
		break;
	case 1:
		base = 16;
		break;
	case 2:
		base = 32;
		break;
	case 3:
		base = 64;
		break;
	case 4:
		base = 96;
		break;
	case 5:
		base = 128;
		break;
	case 6:
		base = 160;
		break;
	case 7:
		base = 192;
		break;
	case 8:
		base = 224;
		break;
	case 9:
		base = b;
		break;
	case 10:
		base = e;
		break;
	case 11:
		base = d;
		break;
	case 12:
		base = (b + e) / 2;
		break;
	case 13:
		base = (2 * b + e) / 3;
		break;
	case 14:
		base = (b + 2 * e) / 3;
		break;
	case 15:
		base = (3 * b + 2 * e) / 5;
		break;
	case 16:
		base = (2 * b + 3 * e) / 5;
		break;
	case 17:
		base = (3 * b + e) / 4;
		break;
	case 18:
		base = (b + 3 * e) / 4;
		break;
	case 19:
		base = (4 * b + e) / 5;
		break;
	case 20:
		base = (b + 4 * e) / 5;
		break;
	case 21:
		base = b + e - d;
		break;
	case 22:
		base = b + (e - d) / 2;
		break;
	case 23:
		base = e + (b - d) / 2;
		break;
	case 24:
		base = d + (b - c);
		break;
	case 25:		// 90 degrees
		base = Velocity(h, e);
		break;
	case 26:		// 112.5 degrees
		base = Velocity(Average(h, g), e);
		break;
	case 27:		// 135 degrees
		base = Velocity(f, d);
		break;
	case 28:		// 157.5 degrees
		base = Velocity(Average(c, a), b);
		break;
	case 29:		// 180 degrees
		base = Velocity(a, b);
		break;
	case 30:
		base = Velocity(g, Average(d, e));
		break;
	default:
		assert(code < NumCodes);
		assert(0);
		break;
	}
	return base;
}


int PredictorEncodeDCTBlock(const uint8_t *srcImg, uint8_t *dstImg, const uint8_t code, const int startRow, const int startCol, const int w, int *overflow)
{
	int err = 0;
	*overflow = 0;
	for (int row = startRow; row < startRow + BlockSize; row++)
	{
		for (int col = startCol; col < startCol + BlockSize; col++)
		{
			int base = GetPredictorBase(srcImg, code&PredCodeMask, row, col, w);
			int res = srcImg[row*w + col] - base;

			err += res*res;

			// offset it so around 127
			res += 127;

			if (res < 0)
			{
				res = 0;
				*overflow |= 1;
			}
			if (res > 255)
			{
				res = 255;
				*overflow |= 2;
			}

			dstImg[row*w + col] = (uint8_t)res;
		}
	}
	return err;
}

void PredictorDecodeBlock(uint8_t *residual, const uint8_t code, uint8_t* image, const int startRow, const int startCol, const int w)
{
	for (int row = startRow; row < startRow + BlockSize; row++)
	{
		for (int col = startCol; col < startCol + BlockSize; col++)
		{
			int base = GetPredictorBase(image, code&PredCodeMask, row, col, w);

			int pix = residual[row*w + col] + base - 127;

			if (pix < 0)
				pix = 0;
			if (pix > 255)
				pix = 255;
			image[row*w + col] = (uint8_t)pix;
		}
	}
}


float PredictorEncodeBlock(uint8_t *image, uint8_t *distortedImage, uint8_t *residual, uint8_t *distortedResidual, uint8_t code, const int row, const int col, const int w, bool bDoLossless)
{
	// copy block of original in to (wip) distorted original, it will get replaced as we decided on this block code and simulate lossy codec
	BlitBlock(image, distortedImage, row, col, w);
	
	int16_t dctCoeff[PredDctRatio][DCTSIZE][DCTSIZE];
	
	float err = 0.0f;
	int dctBank = 0;
	for (int i = 0;i < PredTileBlockSize; i += DCTSIZE)
	{
		for (int j = 0; j < PredTileBlockSize; j += DCTSIZE)
		{
			int overFlow;
			PredictorEncodeDCTBlock(distortedImage, residual, code, row + i, col + j, w, &overFlow);

			// simulate lossy codec on residual image
			
			DCTEncodeBlock(residual, dctCoeff[dctBank], w, row + i, col + j, code);
			DCTDecodeBlock(dctCoeff[dctBank], distortedResidual, w, row + i, col + j, code);
			
			// now that we have a distorted residual, apply the predictor decoding to simulate the 
			// mangled recoverd original, and use this as basis for future predictor encoding
			PredictorDecodeBlock(distortedResidual, code, distortedImage, row + i, col + j, w);
			err += CalculateBlockMSE(image, distortedImage, row + i, col + j, w);

			dctBank++;
		}
	}
	
	if (bDoLossless)
		LosslessEncodeAddBlock(code, (int16_t *)dctCoeff);
	
	return err;
}

void PredictorEncode(uint8_t *image, const int w, const int h)
{
	const int numPredBlocksW = w / PredTileBlockSize;
	const int numPredBlocksH = h / PredTileBlockSize;

	uint8_t* distortedOrig = new uint8_t[w*h];
	uint8_t* controlBits = new uint8_t[numPredBlocksW*numPredBlocksH];
	uint8_t* residual = new uint8_t[w*h];
	uint8_t* distortedResidual = new uint8_t[w*h];
	uint8_t* pCont = controlBits;

	LosslessEncodeInit(w, h);

	for (int row = 0; row < h; row += PredTileBlockSize)
	{
		for (int col = 0; col < w; col += PredTileBlockSize)
		{
			// try all the different preditor types, pick the best
			float bestError = FLT_MAX;
			uint8_t bestCode = 0;

			for (uint8_t code = 0; code < NumCodes; code++)
			{
				for (uint8_t q = 0; q < NumQuantTables; q++)
				{
					uint8_t tmpCode = code|(q<< QuantShift);

					float err = PredictorEncodeBlock(image, distortedOrig, residual, distortedResidual, tmpCode, row, col, w, false);

					if (err < bestError)
					{
						bestError = err;
						bestCode = tmpCode;
					}
				}
			}

			// now code it for real
			PredictorEncodeBlock(image, distortedOrig, residual, distortedResidual, bestCode, row, col, w, true);

			*pCont++ = bestCode;
		}
	}
	LosslessEncodeDo();

	stbi_write_png("temp/residual.png", w, h, 1, residual, w);
	stbi_write_png("temp/control.png", numPredBlocksW, numPredBlocksH, 1, controlBits, numPredBlocksW);

	uint8_t *decoded = PredictorDecode(controlBits, distortedResidual, w, h, distortedOrig);

	delete[] controlBits;
	delete[] residual;
	delete[] decoded;
}

void LosslessPredictorEncode(uint8_t *image, const int w, const int h)
{
	const int numPredBlocksW = w / PredTileBlockSize;
	const int numPredBlocksH = h / PredTileBlockSize;

	uint8_t* controlBits = new uint8_t[numPredBlocksW*numPredBlocksH];
	uint8_t* residual = new uint8_t[w*h];
	uint8_t* pCont = controlBits;

	for (int row = 0; row < h; row += PredTileBlockSize)
	{
		for (int col = 0; col < w; col += PredTileBlockSize)
		{
			// try all the different preditor types, pick the best
			int bestError = INT_MAX;
			uint8_t bestCode = 0;

			for (uint8_t code = 0; code < NumCodes; code++)
			{
				int err = 0;
				for (int i = 0; i < PredTileBlockSize; i += DCTSIZE)
				{
					for (int j = 0; j < PredTileBlockSize; j += DCTSIZE)
					{
						int overFlow;
						err += PredictorEncodeDCTBlock(image, residual, code, row + i, col + j, w, &overFlow);
					}
				}

				if (err < bestError)
				{
					bestError = err;
					bestCode = code;
				}
			}

			*pCont++ = bestCode;
		}
	}
	DumpCodewordStats(controlBits, w*h/(PredTileBlockSize*PredTileBlockSize));

	stbi_write_png("temp/residual.png", w, h, 1, residual, w);
	stbi_write_png("temp/control.png", numPredBlocksW, numPredBlocksH, 1, controlBits, numPredBlocksW);

	delete[] controlBits;
	delete[] residual;
}

uint8_t*  PredictorDecode(uint8_t *control, uint8_t *residual, const int w, const int h, uint8_t *original)
{
    const int numBlocksW = w / PredTileBlockSize;
	
	uint8_t *decoded = new uint8_t[w*h];

	for (int row = 0; row < h; row += BlockSize)
	{
		for (int col = 0; col < w; col += BlockSize)
		{
			uint8_t code = control[(row / PredTileBlockSize)*numBlocksW + col / PredTileBlockSize];

			PredictorDecodeBlock(residual, code, decoded, row, col, w);
		}
	}
	stbi_write_png(outFileName, w, h, 1, decoded, w);

	return decoded;
}


//---------------------------------------------------------------------------------------------------------------
// stats so we can accumulate and look at things over a bunch of images
const int MaxStatImages = 30;
int curStatIdx = 0;
float stdDevList[MaxStatImages] = { 0 };
int codeList[MaxStatImages][NumCodes] = { {0} };

void AccumulateStats(uint8_t *control, uint8_t *residual, const int w, const int h)
{
	uint8_t *c = control;
	for (int i = 0; i < w*h / DCTSIZE2; i++)
		codeList[curStatIdx][*c++]++;

	float avg = 0.0f;
	uint8_t *r = residual;
	for (int i = 0; i < w*h; i++)
		avg += *r++;
	avg /= w*h;

	float stdDev = 0.0f;
	r = residual;
	for (int i = 0; i < w*h; i++)
	{
		stdDev += (*r - avg)*(*r - avg);
		r++;
	}
	stdDev = sqrtf(stdDev / (w*h));

	stdDevList[curStatIdx] = stdDev;

	curStatIdx++;
}

void DumpStats(void)
{
	FILE *fp = fopen("temp/histogram.csv", "wt");

	fprintf(fp,"stdDev ");
	for (int i = 0; i < curStatIdx; i++)
		fprintf(fp, ", %f ", stdDevList[i]);
	fprintf(fp,"\n");

	for (int c = 0; c < NumCodes; c++)
	{
		fprintf(fp, "%d ", c);
		for (int i = 0; i < curStatIdx; i++)
			fprintf(fp, ", %d", codeList[i][c]);
		fprintf(fp, "\n");
	}
	fclose(fp);
}

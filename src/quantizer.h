#pragma once
#include "CommonInclude.h"

extern float gLastQuantError;
extern float gLastWorstQuantError;
extern float gLastHiFreqRatio;
extern int gLastWorstX;
extern int gLastWorstY;

void InitQuantTables(float q);
void QuantizeBlock(float in[][DCTSIZE], int16_t out[][DCTSIZE], uint8_t code);
void DeQuantizeBlock(int16_t in[][DCTSIZE], float out[][DCTSIZE], uint8_t code);
char *GetQuantBankDesc(int bank);
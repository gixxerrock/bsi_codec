#pragma once
#include "CommonInclude.h"

void InitDctTables(void);
void ZigZagCoefficients(int16_t *workspace);
void DCT_float(float in[][DCTSIZE], float out[][DCTSIZE]);
void IDCT_float(float in[][DCTSIZE], float out[][DCTSIZE]);
void BlockByte2Float(uint8_t *srcImg, const int w, const int r, const int c, float out[][DCTSIZE]);
void BlockFloat2Byte(float in[][DCTSIZE], uint8_t *dstImg, const int w, const int r, const int c);


void DCTEncodeBlock(uint8_t *srcImg, int16_t dctCoeff[][DCTSIZE], int w, int r, int c, uint8_t code);
void DCTDecodeBlock(int16_t dctCoeff[][DCTSIZE], uint8_t *dstImg, int w, int r, int c, uint8_t code);

// home brew experimental stuff ( not guaranteed to work at any given time or in the future)
void TestDCTTransforms(uint8_t *image, const int w, const int h);


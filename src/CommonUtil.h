#pragma once
#include "CommonInclude.h"

void BlitBlock(uint8_t *src, uint8_t *dst, const int startRow, const int startCol, const int w);
float CalculateBlockMSE(const uint8_t *img1, const uint8_t *img2, const int startRow, const int startCol, const int w);
float CalculatePSNR(const uint8_t *img1, const uint8_t *img2, const int w, const int h);
float ConvertMSEToPSNR(float a_fMSE);
uint8_t* StripChannel(uint8_t* src, int w, int h, int offset, int skip);

extern int coeffReorder[DCTSIZE2];
extern int coeffReorderExp[NumQuantTables][DCTSIZE2];
void ZigZagCoefficients(int16_t *workspace);

extern float gQFactor;
extern char *outputDir;
extern char *decodeInputDir;
extern char *outFileName;
char *MakeFilename(char *dir, char *file);

void DumpCodewordStats(uint8_t *control, int numBlocks);